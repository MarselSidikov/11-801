package ru.itis;

public class MainBuilder {
    public static void main(String[] args) {
//        User.Builder builder = new User.Builder();
//        User.Builder b1;
//        User.Builder b2;
//        User.Builder b3;
//        User.Builder b4;
//        User.Builder b5;
//
//        b1 = builder.firstName("Марсель");
//        b2 = b1.lastName("Сидиков");
//        b3 = b2.age(26);
//        b4 = b3.height(1.85);
//        b5 = b4.weight(70);
//        builder.firstName("Марсель")
//                .lastName("Сидиков")
//                .age(26)
//                .height(1.85)
//                .weight(70);


//        User user = new User(builder);
//        User user = builder.build();

//        System.out.println(user);

        User user = User.builder()
                .firstName("Марсель")
                .age(7)
                .weight(32)
                .build();

        int i = 0;

    }
}
