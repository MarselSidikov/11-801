package ru.itis;

public class MainStringBuilder {

    public static void main(String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Hello")
                .append("Bye")
                .append("Marsel");

        String text = stringBuilder.toString();
    }
}
