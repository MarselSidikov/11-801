import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        List<String> stringsList = new ArrayList<>();
        stringsList.add("Марсель");
        stringsList.add("Егор");
        stringsList.add("Богдан");
        stringsList.add("Вика");
        stringsList.add("Семен");
        stringsList.add("Ксения");

//        Stream<String> stringsStream = stringsList.stream();
//
//        Function<String, Integer> lengthFunction = line -> line.length();
//
//        Stream<Integer> integersStream = stringsStream.map(lengthFunction);
//
//        Predicate<Integer> evenOrOddPredicate = number -> number % 2 == 0;
//
//        Stream<Integer> evensIntegersStream = integersStream.filter(evenOrOddPredicate);
//
//        Consumer<Integer> printerConsumer = number -> System.out.println(number);
//
//        evensIntegersStream.forEach(printerConsumer);

//        stringsList.stream()
//				.map(line -> line.length())
//				.filter(number -> number % 2 == 0)
//                .forEach(number -> System.out.println(number));

		stringsList.stream()
				.map(String::length)
				.filter(number -> number % 2 == 0)
				.forEach(System.out::println);


    }
}
