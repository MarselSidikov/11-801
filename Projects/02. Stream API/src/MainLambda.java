public class MainLambda {
    public static void main(String[] args) {
//        Printer printer = new Printer() {
//            @Override
//            public void log(String message) {
//                System.out.println("Hello!");
//            }
//        };

        Printer printer = message -> System.out.println("Hello!");

        UsersService service = new UsersService() {
            @Override
            public void signIn(String login, String password) {
                System.out.println("Вы вошли в приложение");
            }

            @Override
            public void signUp(String firstName, String lastName, String login, String password) {
                System.out.println("Вы зарегистрировались");
            }
        };
    }
}
