package ru.itis;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {
	    UsersRepository usersRepository = new UsersRepository();
	    Optional<User> userOptional = usersRepository.findByLogin("Marsel");
//        if (userOptional.isPresent()) {
//            User user = userOptional.get();
//            System.out.println(user.getLogin());
//        } else {
//            System.out.println("Пользователя не нашли");
//        }

        userOptional.ifPresent(user -> System.out.println(user.getLogin()));
        User user = userOptional.orElse(new User("Кто-то"));

    }
}
