package ru.itis;

import java.util.StringJoiner;

public class User {
    private String login;

    public User(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }
}
