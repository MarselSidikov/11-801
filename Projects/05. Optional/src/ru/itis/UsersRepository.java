package ru.itis;

import java.util.Optional;

public class UsersRepository {
    public Optional<User> findByLogin(String login) {
        if (login.equals("Marsel")) {
            return Optional.of(new User("Marsel"));
        } else {
            return Optional.empty();
        }
    }
}
