package ru.itis.springbootdemo.service;

import ru.itis.springbootdemo.dto.UsersSearchResult;

public interface SearchService {
    UsersSearchResult searchUsers(String query, String state, Integer page, Integer size);
}
