public class Terminal {

    private int balance;

    private Printer printer;

    //private SimplePrinter printer;

//    private RedPrinter printer;

//    public Terminal(int balance, SimplePrinter simplePrinter) {
//        this.balance = balance;
//        this.printer = simplePrinter;
//    }

//    public Terminal(int balance, RedPrinter redPrinter) {
//        this.balance = balance;
//        this.printer = redPrinter;
//    }

    public Terminal(int balance, Printer printer) {
        this.balance = balance;
        this.printer = printer;
    }


    public boolean giveMoney(int money) {
        printer.log("Проверка баланса");
        if (balance >= money) {
            printer.log("Выдача денег");
            this.balance -= money;
            return true;
        } else {
            printer.log("Денег нет, но вы держитесь");
            return false;
        }
    }
}
