package ru.itis.base;


public class Main {

    public static void main(String[] args) throws Exception {

        Tirex tirex = new Tirex();
        Thread tirexThread = new Thread(tirex);
        tirexThread.start();

        HenThread henThread = new HenThread();
        EggThread eggThread = new EggThread();
        henThread.start();
        eggThread.start();

        henThread.join();
        eggThread.join();
        tirexThread.join();



        for (int i = 0; i < 1000; i++) {
            System.out.println("Human");
        }

        ThreadsService threadsService = new ThreadsService();

        threadsService.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("Hello!");
            }
        });

        threadsService.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("Bye");
            }
        });
    }
}
