package ru.itis.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainExecutorService {
    public static void main(String[] args) {
        ExecutorService threadPool = Executors.newFixedThreadPool(3);

        threadPool.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("A " + Thread.currentThread().getName());
            }
        });
        threadPool.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("B " + Thread.currentThread().getName());
            }
        });
        threadPool.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("C " + Thread.currentThread().getName());
            }
        });
    }
}
