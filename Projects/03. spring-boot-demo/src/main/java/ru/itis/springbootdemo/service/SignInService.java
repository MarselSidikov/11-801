package ru.itis.springbootdemo.service;

public interface SignInService {
    String signIn(String email, String password);
}
